.. PayMate - A payment API for hackerspaces documentation master file, created by
   sphinx-quickstart on Fri Aug 18 10:17:56 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PayMate - A payment API for hackerspaces's documentation!
====================================================================

PayMate provides a restfull API for handling payments in hackerspaces (drinks or snacks).

The code isn't very clean yet and there might be dozens of security problems.
You are invited to help to get these issues fixed.

When you find an issue, please report it in the `issue tracker <https://chaos.expert/telegnom/paymate/issues>`_!

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   barcodes
