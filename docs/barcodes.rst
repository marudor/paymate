Barcodes
********

PayMate is capable of handling barcodes as specified by GS1 standard GTIN-8, GTIN-12, GTIN-13 and GTIN-14.

.. autoclass:: app.barcode.Barcode()
   :members:
