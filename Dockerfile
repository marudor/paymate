FROM python:3

WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

ENV FLASK_APP=paymate.py
ARG SQLADAPTER
RUN pip install --no-cache-dir $SQLADAPTER
COPY . .
# RUN ["flask", "db", "upgrade"]
CMD ["python", "./paymate.py"]
