# PayMate

## API

API according to the definition from [Space Markets API](https://space-market.github.io/API/)

## State of the API

The goal is to provide a simple app. There might be a rash of security issues, please improve it! The code needs a lot of refactoring and cleanup, too.


## Installation

1. ```git clone https://chaos.expert/telegnom/paymate.git```
1. ```cd paymate```
1. ```pip install -r requirements.txt```
1. adjust settings in ```app/config.py```
1. ```export FLASK_APP=paymate```
1. ```flask db init```
1. ```flask db migrate```
1. ```flask db upgrade```
1. ```flask run```

## Docker

1. ```docker-compose up -d```
1. ```docker-compose run flask db init```
1. ```docker-compose run flask db migrate```
1. ```docker-compose run flask db upgrade```
Optional - only if database outdated

## Upgrade

1. ```git pull```
1. ```export FLASK_APP=paymate```
1. ```flask db upgrade```
1. ```flask run```
