# -*- coding: utf-8 -*-
import os

class Config(object):
    PORT = int(os.getenv('PORT')) if os.getenv('PORT') else 2342
    HOST = os.getenv('HOST') if os.getenv('HOST') else '127.0.0.1'
    GLOBAL_CREDIT_LIMIT = int(os.getenv('CREDIT_LIMIT')) if os.getenv('CREDIT_LIMIT') else 2000
    CURRENCY = '€'
    CURRENCY_BEFORE = False
    DECIMAL_SEPERATOR = ','
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URI') if os.getenv('DATABASE_URI') else 'mysql://paymate:paymate@localhost/paymate'
    UPLOAD_DIR = 'data/images'
    # do not change anything below this line!
    BASEDIR = os.path.abspath(os.path.dirname(__name__))
    VERSION = '3.0.0'
    API_URI_VERSION = 'v3'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEFAULT_RENDERERS = ['flask.ext.api.renderers.JSONRenderer']

    @staticmethod
    def init_app(app):
        pass

class DevelopmentConfig(Config):
    DEBUG = True
    # SQLALCHEMY_DATABASE_URI = 'sqlite:///%sdata.sqlite' % \
    os.path.join(Config.BASEDIR)
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    DEFAULT_RENDERERS = [
        'flask.ext.api.renderers.JSONRenderer',
        'flask.ext.api.renderers.BrowsableAPIRenderer',
    ]


class ProductionConfig(Config):
    DEBUG = False

use_config = ProductionConfig()
