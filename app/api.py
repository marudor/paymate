import json
import magic
import uuid
import os
from flask import jsonify, request, abort
from flask import send_file
from flask_api import status
from flask_api.decorators import set_renderers
from flask_api.renderers import JSONRenderer, BrowsableAPIRenderer
from datetime import datetime
import dateutil.parser
from sqlalchemy.sql import func
from . import app, db
from .models import Product, User, Transaction, Image
from .barcode import Barcode

@app.route('/v3/info/', methods=['GET'])
@app.route('/v3/', methods=['GET'])
def api_info():
    data = {}
    data['api_version'] = 'v3.0.1'
    data['global_credit_limit'] = 2000
    data['currency'] = '€'
    data['currency_before'] = False
    data['decimal_seperator'] = ","
    data['capabilities'] = ['image']
    return jsonify(data)


# Products
@app.route('/{}/products/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_products_get():
    products = Product.query.order_by('name').all()
    data = []
    for prod in products:
        data.append(json.loads(str(prod)))
    return data


@app.route('/{}/products/'.format(app.config['API_URI_VERSION']), methods=['POST'])
def api_products_post():
    prod = Product()
    for attr in prod.patchable_attributes():
        if attr in request.data:
            if attr == 'caffeine' and request.data[attr] not in range(0, 100):
                error = {"error": "caffeine must be in range between 0 and 960"}
                return error, status.HTTP_400_BAD_REQUEST
            if attr == 'alcohol' and request.data[attr] not in range(0, 960):
                error = {"error": "alcohol must be in range between 0 and 960"}
                return error, status.HTTP_400_BAD_REQUEST
            bcode_check = Barcode.check(request.data[attr])
            if attr == 'barcode' and not bcode_check == True:
                error = {"error": bcode_check}
                return error, status.HTTP_400_BAD_REQUEST
            setattr(prod, attr, request.data[attr])
    prod.updated_at = datetime.utcnow()
    prod.created_at = datetime.utcnow()
    try:
        db.session.add(prod)
        db.session.commit()
    except BaseException as e:
        db.session.rollback()
        error = {"error": e}
        return e, status.HTTP_500_INTERNAL_SERVER_ERROR
    return api_products_id_get(prod.id)


@app.route('/{}/products/<int:prod_id>/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_products_id_get(prod_id):
    data = Product.query.get(prod_id)
    if not data:
        return {"error": "product not found"}, status.HTTP_404_NOT_FOUND
    data = json.loads(str(data))
    return data


@app.route('/{}/products/<int:prod_id>/'.format(app.config['API_URI_VERSION']), methods=['PATCH'])
def api_products_id_patch(prod_id):
    if request.method == 'PATCH':
        prod = Product.query.get(prod_id)
        if not prod:
            abort(404)
        for attr in prod.patchable_attributes():
            if attr in request.data:
                if attr == 'caffeine' and request.data[attr] not in range(0, 100):
                    abort(400)
                if attr == 'alcohol' and request.data[attr] not in range(0, 960):
                    abort(400)
                bcode_check = Barcode.check(request.data[attr])
                if attr == 'barcode' and not bcode_check == True:
                    error = {"error": bcode_check}
                    return error, status.HTTP_400_BAD_REQUEST
                setattr(prod, attr, request.data[attr])
        prod.updated_at = datetime.utcnow()
        db.session.add(prod)
        db.session.commit()
        return api_products_id_get(prod_id)


@app.route('/{}/products/<int:prod_id>/'.format(app.config['API_URI_VERSION']), methods=['DELETE'])
def api_products_id_delete(prod_id):
    try:
        db.session.query(Product).filter(Product.id == prod_id).delete()
        db.session.commit()
    except BaseException as e:
        db.session.rollback()
        return {"error": e}, HTTP_500_INTERNAL_SERVER_ERROR
    return {}, status.HTTP_204_NO_CONTENT


# Users
@app.route('/{}/users/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_user_get():
    users = User.query.order_by('name').all()
    data = []
    for user in users:
        data.append(json.loads(str(user)))
    return data


@app.route('/{}/users/'.format(app.config['API_URI_VERSION']), methods=['POST'])
def api_user_post():
    if 'name' not in request.data:
        return {"error": "name required"}, status.HTTP_400_BAD_REQUEST
    user = User()
    for attr in user.patchable_attributes():
        if attr in request.data:
            setattr(user, attr, request.data[attr])
    user.created_at = datetime.utcnow()
    user.updated_at = datetime.utcnow()
    try:
        db.session.add(user)
        db.session.commit()
    except BaseException as e:
        db.session.rollback()
        return {"error": e}, HTTP_500_INTERNAL_SERVER_ERROR

    data = User.query.get(user.id)
    if not data:
        return {"error": "user not found"}, status.HTTP_404_NOT_FOUND
    data = json.loads(str(data))
    return data


@app.route('/{}/users/<int:user_id>/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_user_id_get(user_id):
    data = User.query.get(user_id)
    if not data:
        abort(404)
    data = json.loads(str(data))
    return data


@app.route('/{}/users/<int:user_id>/'.format(app.config['API_URI_VERSION']), methods=['PATCH'])
def api_user_id_patch(user_id):
    user = User.query.get(user_id)
    if not user:
        return {"error": "user not found"}, status.HTTP_404_NOT_FOUND
    for attr in user.patchable_attributes():
        if attr in request.data:
            setattr(user, attr, request.data[attr])
    user.updated_at = datetime.utcnow()
    try:
        db.session.add(user)
        db.session.commit()
    except BaseException as e:
        db.session.rollback()
        return {"error": e}, HTTP_500_INTERNAL_SERVER_ERROR
    return api_user_id_get(user_id)


@app.route('/{}/users/<int:user_id>/'.format(app.config['API_URI_VERSION']), methods=['DELETE'])
def api_user_id_delete(user_id):
    try:
        db.session.query(User).filter(User.id == user_id).delete()
        db.session.commit()
    except BaseException:
        return {"error": "user not found"}, status.HTTP_404_NOT_FOUND
    return {}, status.HTTP_204_NO_CONTENT


@app.route('/{}/users/<int:user_id>/deposit/'.format(app.config['API_URI_VERSION']), methods=['POST'])
def api_user_id_deposit_post(user_id):
    if "amount" not in request.data:
        return {"error": "amount is required"}, status.HTTP_400_BAD_REQUEST
    if not request.data['amount'] > 0:
        return {"error": "amount must be greater than 0"}, status.HTTP_400_BAD_REQUEST
    user = User.query.get(user_id)
    if not user:
        return {"error": "user not found"}, status.HTTP_404_NOT_FOUND
    trans = Transaction()
    trans.created_at = datetime.utcnow()
    trans.action = "deposit"
    trans.amount = request.data['amount']
    user.balance += request.data['amount']
    user.updated_at = datetime.utcnow()
    try:
        db.session.add(user)
        db.session.add(trans)
        db.session.commit()
    except BaseException as e:
        db.session.rollback()
        return {"error": e}, HTTP_500_INTERNAL_SERVER_ERROR
    return {}, status.HTTP_204_NO_CONTENT


@app.route('/{}/users/<int:user_id>/spend/'.format(app.config['API_URI_VERSION']), methods=['POST'])
def api_user_id_spend_post(user_id):
    if "amount" not in request.data:
        return {"error": "amount is required"}, status.HTTP_400_BAD_REQUEST
    user = User.query.get(user_id)
    if not user:
        return {"error": "user not found"}, status.HTTP_404_NOT_FOUND
    trans = Transaction()
    trans.created_at = datetime.utcnow()
    trans.action = "spend"
    trans.amount = request.data['amount']
    user.balance -= request.data['amount']
    user.updated_at = datetime.utcnow()
    try:
        db.session.add(user)
        db.session.add(trans)
        db.session.commit()
    except BaseException as e:
        db.session.rollback()
        return {"error": e}, HTTP_500_INTERNAL_SERVER_ERROR
    return {}, status.HTTP_204_NO_CONTENT


@app.route('/{}/teapot/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_user_teapot_get():
    abort(418)


@app.route('/{}/users/<int:user_id>/buy/'.format(app.config['API_URI_VERSION']), methods=['POST'])
def api_user_id_buy_post(user_id):
    if "product" not in request.data:
        return {"error": "product is required"}, status.HTTP_400_BAD_REQUEST
    user = User.query.get(user_id)
    trans = Transaction()
    prod = Product.query.get(request.data['product'])
    if not user:
        return {"error": "user not found"}, status.HTTP_404_NOT_FOUND
    if not prod:
        return {"error": "product not found"}, status.HTTP_404_NOT_FOUND
    trans.amount = prod.price
    trans.product_id = prod.id
    trans.action = 'spend'
    if user.audit:
        trans.user_id = user.id
    trans.created_at = datetime.utcnow()
    user.balance -= prod.price
    user.updated_at = datetime.utcnow()
    db.session.add(user)
    db.session.add(trans)
    try:
        db.session.commit()
    except BaseException as e:
        db.session.rollback()
        return {"error": e}, HTTP_500_INTERNAL_SERVER_ERROR
    return {}, status.HTTP_204_NO_CONTENT


@app.route('/{}/users/<int:user_id>/buy/barcode/'.format(app.config['API_URI_VERSION']), methods=['POST'])
def api_user_id_buy_barcode_post(user_id):
    if "barcode" not in request.data:
        return {"error": "barcode is required"}, status.HTTP_400_BAD_REQUEST
    user = User.query.get(user_id)
    trans = Transaction()
    prod = Product.query.filter_by(barcode=request.data['barcode']).first()
    if not user:
        return {"error": "user not found"}, status.HTTP_404_NOT_FOUND
    if not prod:
        return {"error": "product not found"}, status.HTTP_404_NOT_FOUND
    trans.amount = prod.price
    trans.product_id = prod.id
    trans.action = 'spend'
    if user.audit:
        trans.user_id = user.id
    trans.created_at = datetime.utcnow()
    user.balance -= prod.price
    user.updated_at = datetime.utcnow()
    db.session.add(user)
    db.session.add(trans)
    try:
        db.session.commit()
    except BaseException as e:
        db.session.rollback()
        return {"error": e}, HTTP_500_INTERNAL_SERVER_ERROR
    return {}, status.HTTP_204_NO_CONTENT


@app.route('/{}/users/stats/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_users_stats_get():
    data = {}
    user = User()
    trans = Transaction()
    data['user_count'] = int(user.count())
    data['active_count'] = int(user.count_active())
    data['balance_sum'] = int(trans.balance_sum())
    return data

# Images
@app.route('/{}/images/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_images_get():
    return {}, status.HTTP_200_OK


@app.route('/{}/images/'.format(app.config['API_URI_VERSION']), methods=['POST'])
def api_images_post():
    if 'image' not in request.files:
        return {"error": "image is required"}, status.HTTP_400_BAD_REQUEST
    image = request.files['image']
    if image.filename == '':
        return {"error": "image is required"}, status.HTTP_400_BAD_REQUEST
    fname = str(uuid.uuid4())
    filename = os.path.join(app.config['UPLOAD_DIR'], fname)
    image.save(filename)
    mtype = magic.from_file(filename, mime=True)
    mimetypes = {
        'image/png': '.png',
        'image/jpeg': '.jpg',
        'image/gif': '.gif'
    }
    if mtype not in mimetypes.keys():
        return {"error": "only png, jpg and gif are allowed"}, \
        status.HTTP_415_UNSUPPORTED_MEDIA_TYPE
    os.rename(filename, filename+mimetypes[mtype])
    fname = fname+mimetypes[mtype]
    img = Image()
    img.filename = fname
    img.created_at = datetime.utcnow()
    db.session.add(img)
    db.session.commit()
    image = Image.query.get(img.id)
    return str(image)


@app.route('/{}/images/<int:img_id>/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_images_id_get(img_id):
    img = Image.query.get(img_id)
    if not img:
        return {"error": "image not found"}, status.HTTP_404_NOT_FOUND
    return json.loads(str(img))


@app.route('/{}/images/<int:img_id>/'.format(app.config['API_URI_VERSION']), methods=['DELETE'])
def api_images_id_delete(img_id):
    img = Image.query.get(img_id)
    if not img:
        return {"error": "image not found"}, status.HTTP_404_NOT_FOUND
    filename = img.filename
    try:
        os.remove(os.path.join(app.config['UPLOAD_DIR'], filename))
        db.session.query(Image).filter(Image.id == img_id).delete()
        db.session.commit()
    except BaseException as e:
        db.session.rollback()
        return {"error": e}, HTTP_500_INTERNAL_SERVER_ERROR
    return {}, status.HTTP_204_NO_CONTENT


@app.route('/{}/images/<int:img_id>/img/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_images_id_img_get(img_id):
    img = Image.query.get(img_id)
    if not img:
        return {"error": "image not found"}, status.HTTP_404_NOT_FOUND
    filename = os.path.join(
        app.config['BASEDIR'],
        app.config['UPLOAD_DIR'],
        img.filename
    )
    mimetype = magic.from_file(filename)
    return send_file(
        filename,
        mimetype=mimetype,
        attachment_filename=img.filename,
        as_attachment=True
    )


@app.route('/{}/audits/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_audits_get():
    if 'user' not in request.args:
        return {"error": "user is required"}, status.HTTP_400_BAD_REQUEST
    if 'start' not in request.args:
        return {"error": "start is required"}, status.HTTP_400_BAD_REQUEST
    if 'end' not in request.args:
        enddate = datetime.now
    else:
        enddate = dateutil.parser.parse(end)

    startdate = dateutil.parser.parse(start)

    if enddate <= startdate:
        return {"error": "start must be before end"}, status.HTTP_400_BAD_REQUEST

    user = User.query.get(request.args.get('user'))
    if not user:
        return {"error": "user not found"}, status.HTTP_404_NOT_FOUND
    if not user.audit:
        return {"error": "user deactiveted audits"}, status.HTTP_401_UNAUTHORIZED
    uid = user.id
    audit_deposit = Transaction.query.filter(
        Transaction.user_id == uid). \
        filter(Transaction.action == 'deposit'). \
        filter(Transaction.created_at >= str(startdate)). \
        filter(Transaction.created_at <= str(enddate)). \
        with_entities(func.sum(Transaction.amount)). \
        first()
    audit_deposit = audit_deposit[0]
    if not audit_deposit:
        audit_deposit = 0
    audit_spend = Transaction.query.filter(
        Transaction.user_id == uid). \
        filter(Transaction.action == 'spend'). \
        filter(Transaction.created_at >= str(startdate)). \
        filter(Transaction.created_at <= str(enddate)). \
        with_entities(func.sum(Transaction.amount)). \
        first()
    audit_spend = audit_spend[0]
    if not audit_spend:
        audit_spend[0] = 0
    audit_sum = audit_deposit + audit_spend
    data = {}
    data['sum'] = int(audit_sum)
    data['payments_sum'] = int(audit_spend)
    data['deposits_sum'] = int(audit_deposit)
    audits = Transaction.query.filter(Transaction.user_id == uid). \
        filter(Transaction.created_at >= str(startdate)). \
        filter(Transaction.created_at <= str(enddate)). \
        order_by(Transaction.created_at)
    data['audits'] = []
    for audit in audits:
        data['audits'].append(json.loads(str(audit)))
    return data
