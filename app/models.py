import pytz
import json
from sqlalchemy.sql import func
from . import db


class Product(db.Model):
    __tablename__ = 'products'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(64), unique=True)
    caffeine = db.Column(db.Integer, default=None)
    alcohol = db.Column(db.Integer, default=None)
    price = db.Column(db.Integer, default=150)
    barcode = db.Column(db.String(128), unique=True)
    created_at = db.Column(db.DateTime)
    updated_at = db.Column(db.DateTime)
    image = db.Column(db.Integer, default=None)
    active = db.Column(db.Boolean, default=True)
    stock = db.Column(db.Integer, default=None)

    def __repr__(self):
        tz = pytz.timezone("Europe/Berlin")
        if self.created_at:
            self.created_at = str(tz.localize(self.created_at))
        else:
            self.created_at = '1970-01-01T00:00:00Z'

        if self.updated_at:
            self.updated_at = str(tz.localize(self.updated_at))
        else:
            self.updated_at = '1970-01-01T00:00:00Z'
        prod = {
            "id": self.id,
            "name": self.name,
            "caffeine": self.caffeine,
            "alcohol": self.alcohol,
            "price": self.price,
            "barcode": self.barcode,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "active": self.active,
            "image": self.image,
            "stock": self.stock
        }
        return json.dumps(prod)

    def attributes(self):
        attrs = [
            "id",
            "name",
            "caffeine",
            "alcohol",
            "price",
            "barcode",
            "created_at",
            "updated_at",
            "active",
            "image",
            "stock"
            ]
        return attrs

    def patchable_attributes(self):
        attrs = [
            "name",
            "caffeine",
            "alcohol",
            "price",
            "barcode",
            "active",
            "image"
            ]
        return attrs


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(64), unique=True)
    email = db.Column(db.String(128), unique=True)
    created_at = db.Column(db.DateTime)
    updated_at = db.Column(db.DateTime)
    balance = db.Column(db.Integer, default=0)
    active = db.Column(db.Boolean, default=True)
    audit = db.Column(db.Boolean, default=False)
    redirect = db.Column(db.Boolean, default=True)
    avatar = db.Column(db.String(36), default=False)

    def count(self):
        count = User.query.with_entities(func.count(User.id).label('count')).first()
        return count[0]

    def count_active(self):
        active = User.query.with_entities(func.count(User.id).label('count')).filter(User.active).first()
        return active[0]

    def __repr__(self):
        tz = pytz.timezone("Europe/Berlin")
        if self.created_at:
            self.created_at = str(tz.localize(self.created_at))
        else:
            self.created_at = '1970-01-01T00:00:00Z'

        if self.updated_at:
            self.updated_at = str(tz.localize(self.updated_at))
        else:
            self.updated_at = '1970-01-01T00:00:00Z'

        user = {
            "id": self.id,
            "name": self.name,
            "email": self.email,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "balance": self.balance,
            "active": self.active,
            "audit": self.audit,
            "redirect": self.redirect,
            "avatar": self.avatar
        }
        return json.dumps(user)

    def attributes(self):
        attrs = [
            "id",
            "name",
            "email",
            "created_at",
            "updated_at",
            "balance",
            "active",
            "audit",
            "redirect",
            "avatar"
        ]
        return attrs

    def patchable_attributes(self):
        attrs = [
            "name",
            "email",
            "active",
            "audit",
            "redirect",
            "avatar"
            ]
        return attrs


class Transaction(db.Model):
    __tablename__ = 'transactions'
    id = db.Column(db.Integer, primary_key=True)
    action = db.Column(db.String(32))
    amount = db.Column(db.Integer)
    created_at = db.Column(db.DateTime)
    product_id = db.Column(db.Integer, db.ForeignKey('products.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    def balance_sum(self):
        balance = User.query.with_entities(func.sum(User.balance).label('balance_sum')).first()
        return balance[0]

    def __repr__(self):
        tz = pytz.timezone("Europe/Berlin")
        if self.created_at:
            self.created_at = str(tz.localize(self.created_at))
        else:
            self.created_at = '1970-01-01T00:00:00Z'

        transaction = {
            "id": self.id,
            "action": self.action,
            "amount": self.amount,
            "created_at": self.created_at,
            "product_id": self.product_id,
            "user_id": self.user_id
        }
        return json.dumps(transaction)

    def attributes(self):
        attrs = [
            "id",
            "action",
            "amount",
            "created_at",
            "product_id",
            "user_id"
        ]
        return attrs


class Image(db.Model):
    __tablename__ = 'images'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    filename = db.Column(db.String(64), unique=True)
    created_at = db.Column(db.DateTime)

    def __repr__(self):
        tz = pytz.timezone("Europe/Berlin")
        if self.created_at:
            self.created_at = str(tz.localize(self.created_at))
        else:
            self.created_at = '1970-01-01T00:00:00Z'
        image = {
            "id": self.id,
            "filename": self.filename,
            "created_at": self.created_at
        }
        return json.dumps(image)
