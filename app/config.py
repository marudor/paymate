# -*- coding: utf-8 -*-
import os


class Config(object):
    PORT = 2342
    GLOBAL_CREDIT_LIMIT = 2000
    CURRENCY = '€'
    CURRENCY_BEFORE = False
    DECIMAL_SEPERATOR = ','
    SQLALCHEMY_DATABASE_URI = 'mysql://paymate:paymate@localhost/paymate'
    UPLOAD_DIR = 'data/images'
    # do not change anything below this line!
    BASEDIR = os.path.abspath(os.path.dirname(__name__))
    VERSION = '3.0.0 alpha'
    API_URI_VERSION = 'v3'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEFAULT_RENDERERS = 'flask.ext.api.renderers.JSONRenderer'


class Development(Config):
    DEBUG = True
    # SQLALCHEMY_DATABASE_URI = 'sqlite:///%sdata.sqlite' % \
    os.path.join(Config.BASEDIR)
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    DEFAULT_RENDERERS = [
        'flask.ext.api.renderers.JSONRenderer',
        'flask.ext.api.renderers.BrowsableAPIRenderer',
    ]
