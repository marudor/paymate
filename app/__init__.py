from flask_api import FlaskAPI
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cors import CORS
import os.path


app = FlaskAPI(__name__)

if os.path.exists('local_config.py'):
    import local_config
    app.config.from_object('local_config.use_config')
else:
    import config
    app.config.from_object('config.use_config')

db = SQLAlchemy(app)
migrate = Migrate(app, db)
CORS(app, resources=r'/*')
app.config['CORS_HEADERS'] = 'Content-Type'

from . import api
